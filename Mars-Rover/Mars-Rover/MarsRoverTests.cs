using System.Diagnostics;
using System.Runtime.Serialization;
using FluentAssertions;
using NUnit.Framework;

namespace Mars_Rover
{
    public enum Orientation
    {
        N,
        E,
        S,
        W
    }
    public class MarsRoverTests
    {
        [Test]
        public void GetRoverLocationTest()
        {
            Rover rover = new Rover();
            rover.Position.Should().Be("0:0:N");
            
        }
        
        [Test]
        public void MoveRover()
        {
            Rover rover = new Rover();
            rover.Command("M");
            rover.Position.Should().Be("0:1:N");   
        }
        
        [Test]
        public void MoveRoverMultipleTimes()
        {
            Rover rover = new Rover();
            rover.Command("MMM");
            rover.Position.Should().Be("0:3:N");   
        }
        
        [Test]
        public void RotateRoverRight()
        {
            Rover rover = new Rover();
            rover.Command("R");
            rover.Position.Should().Be("0:0:E");   
        }
        
        [Test]
        public void RotateRoverLeft()
        {
            Rover rover = new Rover();
            rover.Command("L");
            rover.Position.Should().Be("0:0:W");   
        }
        
        [Test]
        public void RotateRover180Degrees()
        {
            Rover rover = new Rover();
            rover.Command("RR");
            rover.Position.Should().Be("0:0:S");   
        }
        
        [TestCase("MMRRM", "0:1:S")]
        [TestCase("MMMMRRRRMRR", "0:5:S")]
        public void MoveAndRotateInNorthSouthPlan(string command, string position)
        {
            Rover rover = new Rover();
            rover.Command(command);
            rover.Position.Should().Be(position);   
        }
        
        [Test]
        public void MoveEast()
        {
            Rover rover = new Rover();
            rover.Command("RM");
            rover.Position.Should().Be("1:0:E");   
        }
    }

    public class Rover
    {
        private int _y;
        private Orientation _orientation;

        public Rover()
        {
            _y = 0;
            _orientation = Orientation.N;
        }

        public string Position => $"0:{_y}:{_orientation}";


        public void Command(string commandString)
        {
            foreach (var commandChar in commandString)
            {
                if (commandChar == 'M')
                {
                    MoveRover();
                }

                if (commandChar == 'R')
                {
                    if (_orientation == Orientation.W)
                        _orientation = Orientation.N;
                    else
                        _orientation++;
                }
                
                if (commandChar == 'L')
                {
                    if (_orientation == Orientation.N)
                        _orientation = Orientation.W;
                    else
                        _orientation--;
                }

            }
        }

        private void MoveRover()
        {
            _y += _orientation switch
            {
                Orientation.N => 1, 
                Orientation.S => -1 
            };
        }
    }
}